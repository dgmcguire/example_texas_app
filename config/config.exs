# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :phoenix, :template_engines,
  tex:  Texas.TemplateEngine
config :texas, pubsub: WorkingOnDocsWeb.Endpoint
config :texas, router: WorkingOnDocsWeb.Router

# Configures the endpoint
config :working_on_docs, WorkingOnDocsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "7Uj0jfZkXrI/oVNyf9j059OMGhoHpyucu5Qbw2bVWwJFYz6Ufm7Wru/ADbtvIRVc",
  render_errors: [view: WorkingOnDocsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: WorkingOnDocs.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
